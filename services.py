import concurrent.futures
import csv
import multiprocessing as mp
import os
import time
import zipfile

from io import StringIO
from lxml import etree



def get_new_xml(xml_files_dir, xml_file_name, id, level, objects_list):
    '''
    xml file generator
    '''
    # print((xml_file_name, id, level, objects_list))
    if not os.path.exists(xml_files_dir):
        os.mkdir(xml_files_dir)
    root = etree.Element('root')
    var_id = etree.SubElement(root, 'var', name='id', value=id)
    var_level = etree.SubElement(root, 'var', name='level', value=str(level))
    objects = etree.SubElement(root, 'objects')
    for obj_item in objects_list:
        obj = etree.SubElement(objects, 'object', name=obj_item)

    tree = etree.ElementTree(root)

    xml_file = os.path.join(xml_files_dir, xml_file_name)

    tree.write(xml_file,
               pretty_print=True,
               xml_declaration=True,
               encoding="utf-8")
    return xml_file


# slow reader


def slow_read_zip(zip_files_dir, csv_files_dir):
    '''
    Zip files slow reade
    '''
    start_time = time.time()
    if not os.path.exists(csv_files_dir):
        os.mkdir(csv_files_dir)
    zip_files_list = os.listdir(zip_files_dir)
    for zip_file_item in zip_files_list:
        list_1 = []
        list_2 = []
        #print(zip_file_item)

        zip_file = zipfile.ZipFile(os.path.join(zip_files_dir, zip_file_item))
        zip_file_xml_list = zip_file.namelist()
        for xml_file in zip_file_xml_list:
            xml_data = zip_file.read(xml_file)
            root = etree.fromstring(xml_data)

            var_id = root.xpath("//var[@name='id']")[0]
            id = var_id.get('value')
            var_level = root.xpath("//var[@name='level']")[0]
            level = var_level.get('value')
            list_1.append([id, level])
            objects = root.xpath('//objects')[0]
            for obj in objects.findall('object'):
                list_2.append([id, obj.get('name')])

    with open(os.path.join(csv_files_dir, '1.csv'), 'a') as csv1:
        writer = csv.writer(csv1, delimiter='\t')
        for item in list_1:
            writer.writerow(item)

    with open(os.path.join(csv_files_dir, '2.csv'), 'a') as csv2:
        writer = csv.writer(csv2, delimiter='\t')
        for item in list_2:
            writer.writerow(item)
    print('\nElapsed time for slow zipfiles read, sec: {}'.format(
        time.time() - start_time)
    )

# futures multiprocessing


def multi_read_zip(zip_files_dir, csv_files_dir):
    '''
    Zip files futures multiprocessing
    '''
    start_time = time.time()
    concurrency = mp.cpu_count()
    if not os.path.exists(csv_files_dir):
        os.mkdir(csv_files_dir)
    zip_files_list = os.listdir(zip_files_dir)
    summary = manager_workers(zip_files_dir,
                              zip_files_list,
                              csv_files_dir,
                              concurrency)
    print('Result files:', summary[1])
    print(('\nElapsed time for futures multiprocessing  zipfiles read, '
           'sec: {}').format(time.time() - start_time))


def manager_workers(zip_files_dir,
                    zip_files_list,
                    csv_files_dir,
                    concurrency):
    futures = set()
    with concurrent.futures.ProcessPoolExecutor(
            max_workers=concurrency) as executor:
        for zip_file_name in zip_files_list:
            future = executor.submit(worker,
                                     zip_files_dir,
                                     zip_file_name,
                                     csv_files_dir)
            futures.add(future)
        summary = wait_for(futures)
        if summary[0]:
            executor.shutdown()
        return summary


def worker(zip_files_dir, zip_file_name, csv_files_dir):
    list_1 = []
    list_2 = []
    zip_file = zipfile.ZipFile(os.path.join(zip_files_dir, zip_file_name))
    zip_file_xml_list = zip_file.namelist()
    for xml_file in zip_file_xml_list:
        xml_data = zip_file.read(xml_file)
        root = etree.fromstring(xml_data)

        var_id = root.xpath("//var[@name='id']")[0]
        id = var_id.get('value')
        var_level = root.xpath("//var[@name='level']")[0]
        level = var_level.get('value')
        list_1.append([id, level])
        objects = root.xpath('//objects')[0]
        for obj in objects.findall('object'):
            list_2.append([id, obj.get('name')])

    with open(os.path.join(csv_files_dir, '1.csv'), 'a') as csv1:
        writer = csv.writer(csv1, delimiter='\t')
        for item in list_1:
            writer.writerow(item)

    with open(os.path.join(csv_files_dir, '2.csv'), 'a') as csv2:
        writer = csv.writer(csv2, delimiter='\t')
        for item in list_2:
            writer.writerow(item)
    return csv1.name, csv2.name

def wait_for(futures):
    canceled = False
    try:
        for future in concurrent.futures.as_completed(futures):
            err = future.exception()
            if err is None:
                result = future.result()
            else:
                raise err # Unanticipated
    except KeyboardInterrupt:
        print("canceling...")
        canceled = True
        for future in futures:
            future.cancel()
    return canceled, result
