import argparse
import os
import random
import string
import time
import zipfile

import services


BASE_DIR = os.path.dirname(os.path.abspath(__file__))
zip_files_dir = os.path.join(BASE_DIR, 'zip_files/')
xml_files_dir = os.path.join(BASE_DIR, 'xml_files/')
csv_files_dir = os.path.join(BASE_DIR, 'csv_files/')
ASCII_list = list(
    string.ascii_uppercase + string.ascii_lowercase + string.digits
)
id_list = ['']


def _get_new_id():
    '''
    Get new unique id
    '''
    id = ''
    while id in id_list:
        id = ''.join(random.choice(ASCII_list) for _ in range(10))
    return id


def gen_zip(num_zip, num_xml):
    '''
    Zip files generator
    '''
    start_zip_time = time.time()
    if not os.path.exists(zip_files_dir):
        os.mkdir(zip_files_dir)

    for zip_item in range(num_zip):
        zip_file_name = f'{zip_item}.zip'
        with zipfile.ZipFile(
            os.path.join(zip_files_dir, zip_file_name),
            'w'
        ) as filezip:
            for xml_item in range(num_xml):
                objects_list = []
                for obj_item in range(random.randint(1, 10)):
                    objects_list.append(
                        ''.join(random.choice(ASCII_list) for x in range(20))
                    )
                xml_file_name = f'{zip_item}_{xml_item}.xml'

                xml_file = services.get_new_xml(xml_files_dir,
                                                xml_file_name,
                                                _get_new_id(),
                                                random.randint(1, 100),
                                                objects_list)
                filezip.write(xml_file, xml_file_name)
                # замедляет на 0,08 sec
                # os.remove(xml_file)
    print('\nElapsed time for zipfiles generation, sec: {}'.format(
        time.time() - start_time)
    )


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Num xml files and num zip files'
    )
    parser.add_argument('num_zip',
                        type=int,
                        help='The number of zip file to generate')
    parser.add_argument('num_xml',
                        type=int,
                        help='The number of xml file to generate in zip')
    parser.add_argument('-s',
                        '--slow',
                        action="store_true",
                        help='Enable slow work')
    args = parser.parse_args()
    if args.num_zip and args.num_xml:
        start_time = time.time()
        print('*' * 5,
              (f'Start doca test for {args.num_xml} xml files in each '
               f'{args.num_zip} zip files'))

        gen_zip(args.num_zip, args.num_xml)

        if args.slow:
            services.slow_read_zip(zip_files_dir, csv_files_dir)
        else:
            services.multi_read_zip(zip_files_dir, csv_files_dir)

        print('\nElapsed time, sec: {}'.format(time.time() - start_time))
